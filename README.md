# Linked to elseware

Stuff copied from other places (e.g., my lessons) that I want to link to somewhere (e.g., youtube) but I don't want the link to break later when I update the original version.